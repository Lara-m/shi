var express = require('express'),
	app = new express(),
	url = require("url"),
	path = require("path"),
	pg = require("pg");
var port = process.env.PORT || 5000;
var host = process.env.HOST || "127.0.0.1";
var bodyParser = require('body-parser');

app.listen(port);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));


var conString='postgres://mjjqnmwvjuwvbq:kaIzCovv0txfdxuT7M0MIW5i8b@ec2-54-197-241-67.compute-1.amazonaws.com:5432/dcjg2kvfn7g9ok';
pg.connect(conString, function(err, client) {
	var query  = client.query("CREATE TABLE IF NOT EXISTS DATES(NAME varchar(64), BD varchar(64),STAMP varchar(64), DAYS varchar(64));");
	var query2 = client.query("CREATE TABLE IF NOT EXISTS DATES2(NAME varchar(64), BD varchar(64),STAMP varchar(64));");
	var query3 = client.query("CREATE TABLE IF NOT EXISTS STAMP(TSTAMP varchar(64));");
});

app.get('/', function(req, res) { 
	res.sendFile(__dirname + '/home.html'); 
});

app.post('/resp',function(req,res){
	pg.connect(conString, function(err, client) {
		var query  = client.query("INSERT INTO DATES(NAME,BD,STAMP, DAYS) VALUES('"+req.body.n+ "','" +req.body.bd + "','" +req.body.tstamp+"','"+req.body.result+"');");
		var query2 = client.query("INSERT INTO DATES2(NAME,BD) VALUES('"+req.body.n+ "','" +req.body.bd +"');");
		var query3 = client.query("INSERT INTO STAMP(TSTAMP) VALUES('"+req.body.tstamp+"');");
	});
	res.end();
});

app.post('/logs',function(req,res){
	pg.connect(conString, function(err, client) {
		var str ="";
		var query = client.query("SELECT * FROM DATES2;");
		query.on('row', function(row) {
			console.log(row);
			str = str + "\n" +JSON.stringify(row);
		});
		query.on('end', function(result) {
			res.end(str);
		});
	});
});


/*
*this is using the old table

app.post('/logs',function(req,res){
	pg.connect(conString, function(err, client) {
		var str ="";
		var query = client.query("SELECT * FROM DATES;");
		query.on('row', function(row) {
			console.log(row);
			str = str + "\n" +JSON.stringify(row);
		});
		query.on('end', function(result) {
			res.end(str);
		});
	});
});
*/

/*
*
*
* DO NOT DELETE THIS!
* This is the script used to migrate all data from old DB to new DB design.
*
*
*
pg.connect(conString, function(err, client) {
	var query0 = client.query("DROP TABLE DATES2;");
	var query1 = client.query("DROP TABLE STAMP");
	var query2 = client.query("CREATE TABLE IF NOT EXISTS DATES2(NAME varchar(64), BD varchar(64));");
	var query3 = client.query("CREATE TABLE IF NOT EXISTS STAMP(TSTAMP varchar(64));");

	var query = client.query("SELECT * FROM DATES;");
	query.on('row', function(row) {
		var query2 = client.query("INSERT INTO DATES2(NAME,BD) VALUES('"+row.name+ "','" +row.bd +"');");
		var query3 = client.query("INSERT INTO STAMP(TSTAMP) VALUES('"+row.stamp+"');");
		console.log(JSON.stringify(JSON.stringify(row)));
	});
});
/*
*
*
*/

